import { Link } from "react-router-dom";
import { useState, useContext } from "react";
import { UserContext } from "../context/UserContext";
import { useNavigate } from "react-router-dom";
import Cookies from "../../node_modules/js-cookie";
import { jwtDecode } from "../../node_modules/jwt-decode";

export default function FormLogin() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);

  //Dati del form
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  //Per navigare tra le pagine
  const navigate = useNavigate();

  //Gestione dati
  function handleChange(e) {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  }

  //Invio dati
  const handleSubmit = async (e) => {
    e.preventDefault();

    if (
      formData.email.match("[A-z0-9\\.\\+_-]+@[A-z0-9\\._-]+\\.[A-z]{2,20}") &&
      formData.password.match(
        "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}"
      )
    ) {
      const response = await fetch("http://localhost:8080/api/utente/login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        const tokenResponse = await response.json();
        const token = tokenResponse.token;
        const decodToken = jwtDecode(token);
        //setToken(token); 

        //Recupero i dati
        const nome = decodToken.nome;
        const cognome = decodToken.cognome;
        const email = decodToken.email;
        const ruoli = decodToken.ruoli;

        //Setto i dati
        setUserData({...userData, nome: nome, cognome: cognome, email: email});

        //Li memorizzo nei Cookies
        Cookies.set("nome", nome, { expires: 31 });
        Cookies.set("cognome", cognome, { expires: 31 });
        Cookies.set("email", email, { expires: 31 });
        Cookies.set("ruoli", ruoli, { expires: 31 });
        //
        Cookies.set("token", token, { expires: 31 });

        setIsLogged(true);
        setIsAdmin(ruoli.includes("Admin")); //Imposto isAdmin in base al ruolo Admin
        alert("Login riuscito.");
        //Dopo aver effettuato la login si apre la pagina del profilo utente
        navigate("/userProfile");
      } else {
        alert("Errore durante l'accesso.");
      }
    } else {
      alert("Dati errati, controlla email o password.");
    }
  };

  return (
    <div className="d-flex align-items-center justify-content-center">
      <div className="text-center" style={{ width: "100%", maxWidth: "320px" }}>
        <h2 className="mb-4" style={{ color: "white" }}>
          Accedi
        </h2>
        <form onSubmit={handleSubmit}>
          <div className="form-group mb-3">
            <input
              type="email"
              className="form-control"
              name="email"
              placeholder="Email"
              required
              value={formData.email}
              onChange={handleChange}
            />
          </div>
          <div className="form-group mb-4">
            <input
              type="password"
              className="form-control"
              name="password"
              placeholder="Password"
              required
              value={formData.password}
              onChange={handleChange}
            />
          </div>
          <button type="submit" className="btn btn-primary w-100 mb-2">
            Login
          </button>
          <p className="mb-1">Non sei registrato?</p>
          <Link to="/userRegister" className="text-primary">
            Registrati adesso
          </Link>
          <hr />
          <Link to="/" className="text-primary">
            Torna alla homepage
          </Link>
        </form>
      </div>
    </div>
  );
}
