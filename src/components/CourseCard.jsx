import { Link } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "../context/UserContext";

//Stampo dinamicamente dei sample di corsi accattivanti per la homepage
const corsi = [
  {
    nome: "FullStack Developer",
    testo:
      "Il corso 'FullStack Developer' offre un'introduzione completa allo sviluppo software, coprendo sia il front-end che il back-end delle applicazioni web. " +
      "Gli studenti impareranno linguaggi come HTML, CSS, JavaScript e framework come React o Node.js, preparandoli per una carriera versatile nell'ambito dello sviluppo web.",
  },
  {
    nome: "Machine Learning & IA",
    testo:
      "Il corso di 'Machine Learning & IA' fornisce una formazione su algoritmi di intelligenza artificiale e tecniche di ML. " +
      "Gli studenti impareranno linguaggi di programmazione come Python e librerie come TensorFlow per sviluppare modelli predittivi, algoritmi di apprendimento automatico e sistemi di intelligenza artificiale.",
  },
  {
    nome: "Data Analyst",
    testo:
      "Il corso 'Data Analyst' fornisce competenze essenziali per analizzare e interpretare dati. Gli studenti impareranno tecniche di estrazione, pulizia e visualizzazione dei dati utilizzando strumenti come " +
      "Python, SQL e Excel. Il corso prepara gli studenti a svolgere ruoli chiave nell'analisi dei dati in diversi settori.",
  },
];

export default function CourseCard() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);

  //Se l'utente è loggato ed è Admin, il bottone presente in ogni card-corso porta alla pagina dei corsi
  // altrimenti se l'utente non è loggato e/o non è admin invia due alert differenti
  const handleButtonClick = () => {
    if (!isLogged) {
      alert("Per visualizzare i corsi, effettua prima la login");
    } else if (!isAdmin) {
      alert("Non hai i privilegi per poter accedere ai corsi");
    }
  };

  return (
    <div className="container">
      <h1 style={{ color: "white" }}>I nostri corsi più seguiti</h1>
      <br />
      <div className="row justify-content-center">
        {corsi.map((corso, index) => (
          <div className="col-md-4 mb-4" key={index}>
            <div
              className="card text-center"
              style={{ backgroundColor: "rgba(255, 255, 255, 0.3)" }}
            >
              <div className="card-body">
                <h5 className="card-title">{corso.nome}</h5>
                <p className="card-text">{corso.testo}</p>
                {isLogged && isAdmin ? (
                  <Link to="/courses" className="btn btn-dark">
                    Vai al corso
                  </Link>
                ) : (
                  <button onClick={handleButtonClick} className="btn btn-dark">
                    Vai al corso
                  </button>
                )}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
