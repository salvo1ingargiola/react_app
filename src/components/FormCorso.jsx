import { Link, useNavigate } from "react-router-dom";
import { useContext, useState, useEffect } from "react";
import { UserContext } from "../context/UserContext";
import Cookies from "../../node_modules/js-cookie";

export default function FormCorso() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);

  const [docenti, setDocenti] = useState([]); //Tengo traccia dei docenti
  const [categories, setCategories] = useState([]); //Tengo traccia delle categorie
  const [formCorso, setFormCorso] = useState({
    //Tengo traccia dei dati del form
    nome_corso: "",
    descrizione_breve: "",
    descrizione_completa: "",
    durata: "",
    id_doc: "",
    categoria: {
      id_ca: "",
      nome_categoria: "",
    },
  });

  useEffect(() => {
    async function fetchCategories() {
      //Fetch che restituisce la lista delle categorie
      const response = await fetch(
        "http://localhost:8080/api/categoria/getAll"
      );
      if (response.ok) {
        const data = await response.json();
        setCategories(data);
      } else {
        console.log("Errore nel fetch delle categorie");
      }
    }

    async function fetchDocenti() {
      //Fetch che restituisce la lista degli utenti con idRuolo 2 ovvero docenti
      const response = await fetch(
        "http://localhost:8080/api/utente/getUtenti?id_r=2"
      );
      if (response.ok) {
        const data = await response.json();
        setDocenti(data);
      } else {
        console.log("Errore nel fetch dei docenti");
      }
    }

    fetchCategories();
    fetchDocenti();
  }, []);

  const navigate = useNavigate();

  function handleChange(e) {
    const { name, value } = e.target;

    if (name === "durata") {
      const durataInt = parseInt(value); //Converto la stringa durata in un intero
      setFormCorso({ ...formCorso, [name]: durataInt });
    }
    if (name === "categoria") {
      const [id_ca, nome_categoria] = value.split("_");
      setFormCorso({
        ...formCorso,
        categoria: { id_ca: parseInt(id_ca), nome_categoria }, //Converto la stringa dell'id_categoria in un intero
      });
    } else {
      setFormCorso({ ...formCorso, [name]: value });
    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    const response = await fetch("http://localhost:8080/api/corso", {
      //Fetch che aggiunge un corso
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + Cookies.get("token"),
      },
      body: JSON.stringify(formCorso),
    });

    //console.log("Cookie registrazione corso: " + Cookies.get("token"));

    if (response.ok) {
      alert("Corso aggiunto con successo.");
      navigate("/courses");
    } else {
      alert("Errore nell'aggiunta del corso.");
    }
  };

  //Se l'utente non è loggato e non è admin viene restituito un messaggio "Non hai le autorizzazioni valide per accedere a questa pagina" + un link per tornare alle home
  //Se l'utente è loggato ed è admin viene visualizzato il form per inserire un nuovo corso, con le select che fanno riferimento ai docenti e categorie presenti nel db
  return (
    <div>
      {isLogged && isAdmin ? (
        <div className="container d-flex flex-column align-items-center justify-content-center">
          <h1 style={{ color: "white" }}>Nuovo corso</h1>
          <br />
          <div className="w-100">
            <form
              className="d-flex flex-column align-items-center"
              onSubmit={handleSubmit}
            >
              <input
                type="text"
                name="nome_corso"
                className="form-control mb-2"
                placeholder="Nome Corso"
                required
                value={formCorso.nome_corso}
                onChange={handleChange}
              />
              <input
                type="text"
                name="descrizione_breve"
                className="form-control mb-2"
                placeholder="Descrizione Breve"
                required
                value={formCorso.descrizione_breve}
                onChange={handleChange}
              />
              <input
                type="text"
                name="descrizione_completa"
                className="form-control mb-2"
                placeholder="Descrizione Completa"
                required
                value={formCorso.descrizione_completa}
                onChange={handleChange}
              />
              <input
                type="number"
                name="durata"
                className="form-control mb-2"
                placeholder="Durata in ore"
                required
                value={formCorso.durata}
                onChange={handleChange}
              />

              <select
                name="id_doc"
                className="form-control mb-2"
                value={formCorso.id_doc}
                onChange={handleChange}
              >
                <option value="default1">---- Scegli un docente ----</option>
                {docenti.map((docente) => (
                  <option key={docente.id} value={docente.id}>
                    {docente.nome} {docente.cognome}
                  </option>
                ))}
              </select>

              <select
                name="categoria"
                className="form-control mb-2"
                value={`${formCorso.categoria.id_ca}_${formCorso.categoria.nome_categoria}`}
                onChange={handleChange}
              >
                <option value="default2">---- Scegli una categoria ----</option>
                {categories.map((categoria) => (
                  <option
                    key={categoria.id_ca}
                    value={`${categoria.id_ca}_${categoria.nome_categoria}`}
                  >
                    {categoria.nome_categoria}
                  </option>
                ))}
              </select>

              <button
                type="submit"
                className="btn btn-primary mb-2"
                style={{ width: "100%" }}
              >
                Aggiungi
              </button>
              <Link
                to="/courses"
                className="btn btn-light mt-2"
                style={{ width: "100%" }}
              >
                Torna ai corsi
              </Link>
            </form>
          </div>
        </div>
      ) : (
        <div>
          <p>Non hai le autorizzazioni valide per accedere a questa pagina.</p>
          <Link to="/" className="btn btn-dark">
            Torna alla home
          </Link>
        </div>
      )}
    </div>
  );
}
