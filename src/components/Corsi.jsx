import Cookies from "../../node_modules/js-cookie";
import { UserContext } from "../context/UserContext";
import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Corsi() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);
  const [corsi, setCorsi] = useState([]); //Tengo traccia dei corsi

  useEffect(() => {
    async function fetchData() { 
      const response1 = await fetch( //Fetch per recuperare i corsi di categoria 1
        "http://localhost:8080/api/corso/getcorsi-da?id_ca=1",
        {
          method: "GET",
          headers: {
            Authorization: 'Bearer ' + Cookies.get("token"),
          },
        }
      );
      const response2 = await fetch( //Fetch per recuperare i corsi di categoria 2
        "http://localhost:8080/api/corso/getcorsi-da?id_ca=2",
        {
          method: "GET",
          headers: {
            Authorization: 'Bearer ' + Cookies.get("token"),
          },
        }
      );
      const response3 = await fetch( //Fetch per recuperare i corsi di categoria 3
        "http://localhost:8080/api/corso/getcorsi-da?id_ca=3",
        {
          method: "GET",
          headers: {
            Authorization: 'Bearer ' + Cookies.get("token"),
          },
        }
      );
      const response4 = await fetch( //Fetch per recuperare i corsi di categoria 4
        "http://localhost:8080/api/corso/getcorsi-da?id_ca=4",
        {
          method: "GET",
          headers: {
            Authorization: 'Bearer ' + Cookies.get("token"),
          },
        }
      );
      const response5 = await fetch( //Fetch per recuperare i corsi di categoria 5
        "http://localhost:8080/api/corso/getcorsi-da?id_ca=5",
        {
          method: "GET",
          headers: {
            Authorization: 'Bearer ' + Cookies.get("token"),
          },
        }
      );
      if (!response1.ok && !response2.ok && !response3.ok && !response4.ok && !response5.ok) {
        console.log("Errore");
      } else {
        const data1 = await response1.json();
        const data2 = await response2.json();
        const data3 = await response3.json();
        const data4 = await response4.json();
        const data5 = await response5.json();
        setCorsi(data1.concat(data2).concat(data3).concat(data4).concat(data5));
      }
    }
    fetchData();
  }, []);

  async function deleteCorso(idCorso) { //Fetch per cancellare un corso
    console.log("Fetch: http://localhost:8080/api/corso/delete/" + idCorso);
    const response = await fetch(
      "http://localhost:8080/api/corso/delete/" + idCorso, //Endpoint aggiunto
      {
        headers: {"Authorization": "Bearer " + Cookies.get("token")},
        method: "DELETE",
      }
    );

    if (response.ok) {
      setCorsi(corsi.filter((corso) => corso.id_c !== idCorso));
      alert("Corso cancellato con successo.");
    } else {
      alert("Errore nella cancellazione del corso.");
    }
  }

  //Se l'utente non è loggato e non è admin, viene visualizzato un messaggio "Non hai le autorizzazioni valide" + il link per tornare alla home
  //Se l'utente è loggato ed è admin, viene visualizzata l'intera lista dei corsi attivi
  return (
    <div>
      <h1 className="mt-4" style={{ color: "white" }}>
        Corsi attivi
      </h1>
      {isAdmin && isLogged ? (
        <div className="container">
          <div className="row">
            {corsi.map((corso, index) => (
              <div key={index} className="col-md-4 mb-4">
                <div
                  className="card border-0"
                  style={{ backgroundColor: "rgba(255, 255, 255, 0.3)" }}
                >
                  <div className="card-body text-center">
                    <h5 className="card-title">{corso.nome_corso}</h5>
                    <p className="card-text">{corso.descrizione_breve}</p>
                    <p className="card-text">{corso.descrizione_completa}</p>
                    <p className="card-text">Durata: {corso.durata} gg</p>
                    <button
                      className="btn btn-warning"
                      onClick={() => deleteCorso(corso.id_c)}
                    >
                      Cancella corso
                    </button>
                  </div>
                </div>
              </div>
            ))}
            <div className="mt-3">
              <Link to="/addCourse" className="btn btn-success me-2">
                Aggiungi un corso
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <p>Non hai le autorizzazioni valide</p>
          <Link to="/" className="btn btn-dark">
            Torna alla home
          </Link>
        </div>
      )}
    </div>
  );
}
