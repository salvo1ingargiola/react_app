import FormLogin from "../components/FormLogin";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../context/UserContext";

export default function CardProfile() {
  const [userData, setUserData, isLogged, setIsLogged, isAdmin, setIsAdmin] = useContext(UserContext);
  const [ruoli, setRuoli] = useState([]); //Tengo traccia dei ruoli

  useEffect(() => { //useEffect per eseguire operazioni al cambio di stato di isLogged o userData.email
    async function fetchUser() {
      const response = await fetch( //Fetch che recupera la lista di tutti gli utenti
        "http://localhost:8080/api/utente/getUtenti"
      );
      if (!response.ok) {
        console.log("Errore");
      }
      const data = await response.json();
      //Cerco tra gli utenti quello con l'email corrispondente
      const match = data.find((utente) => utente.email === userData.email);
      //Se trovo una corrispondenza, aggiorno lo stato dei ruoli con i ruoli dell'utente
      if (match) {
        setRuoli(match.ruoli.map((ruolo) => ruolo.tipologia));
      }
    } //Eseguo la fetchUser solo se l'utente è loggato
    if (isLogged) {
      fetchUser();
    }
  }, [isLogged, userData.email]);

  //Se l'utente è loggato mostro la card con il suo profilo, altrimenti mostro il form di login
  return (
    <>
      <h1 className="card-title" style={{ color: "white" }}>
        Benvenuto al profilo utente
      </h1>
      <br />
      {isLogged ? (
        <div className="container">
          <div
            className="card"
            style={{ backgroundColor: "rgba(255, 255, 255, 0.3)" }}
          >
            <div className="card-body">
              <p className="card-text">
                <strong>Nome:</strong> {userData.nome}
              </p>
              <p className="card-text">
                <strong>Cognome:</strong> {userData.cognome}
              </p>
              <p className="card-text">
                <strong>Email:</strong> {userData.email}
              </p>
              <p className="card-text">
                <strong>Ruoli:</strong> {ruoli.join(", ")}
              </p>
              <p className="card-text">
                <strong>Corsi:</strong>
              </p>
            </div>
          </div>
        </div>
      ) : (
        <FormLogin />
      )}
    </>
  );
}
