import { Link, useNavigate } from "react-router-dom";
import { useState, useContext } from "react";
import { UserContext } from "../context/UserContext";

export default function FormRegister() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);

  //Dati del form
  const [formData, setFormData] = useState({
    nome: "",
    cognome: "",
    email: "",
    password: "",
  });

  //Per navigare tra le pagine
  const navigate = useNavigate();

  //Gestione dati
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  //Invio dati
  const handleSubmit = async (e) => {
    e.preventDefault();

    if (
      formData.nome.match("[a-zA-Zàèìòù]{1,50}") &&
      formData.cognome.match("[a-zA-Zàèìòù]{1,50}") &&
      formData.email.match("[A-z0-9\\.\\+_-]+@[A-z0-9\\._-]+\\.[A-z]{2,20}") &&
      formData.password.match(
        "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,20}"
      )
    ) {
      const response = await fetch("http://localhost:8080/api/utente/registrazione", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(formData),
        }
      );

      if (response.ok) {
        alert("Registrazione completata con successo.");
        if(isLogged && isAdmin){ //Se arrivi a questa pagina mentre sei loggato e sei admin vuol dire che stai aggiungendo un nuovo utente
          navigate("/users"); //Quindi dopo aver inviato i dati, reinderizza direttamente alla pagina della lista utenti
        }
        else{ //Altrimenti se arrivi a questa pagina come utente non registrato
          //setIsLogged(true); //Setto la login
          setIsAdmin(false); //Alla registrazione, l'utente non è mai admin, per essere admin deve essere settato manualmente sul db
          //Dopo aver effettuato la registrazione, viene aperta la pagina di login
          navigate("/login");
        }
        
      } else {
        alert("Errore durante la registrazione.");
      }
    } else {
      console.log(e);
      alert("Dati errati.");
    }
  };

  //Se l'utente è registrato ed è admin, se si trova qui è solo per aggiungere un nuovo utente, quindi nascondo il link che riporta alla login
  //Se l'utente non è registrato, ha la possibilità di tornare alla schermata della login
  return (
    <div className="container d-flex flex-column align-items-center justify-content-center">
      <h1 style={{ color: "white" }}>Registrazione</h1>
      <br />
      <div className="w-100">
        <form
          className="d-flex flex-column align-items-center"
          onSubmit={handleSubmit}
        >
          <input
            type="text"
            name="nome"
            className="form-control mb-2"
            placeholder="Nome"
            required
            value={formData.nome}
            onChange={handleChange}
          />
          <input
            type="text"
            name="cognome"
            className="form-control mb-2"
            placeholder="Cognome"
            required
            value={formData.cognome}
            onChange={handleChange}
          />
          <input
            type="email"
            name="email"
            className="form-control mb-2"
            placeholder="Email"
            required
            value={formData.email}
            onChange={handleChange}
          />
          <input
            type="password"
            name="password"
            className="form-control mb-2"
            placeholder="Password"
            required
            value={formData.password}
            onChange={handleChange}
          />
          <button
            type="submit"
            className="btn btn-primary mb-2"
            style={{ width: "100%" }}
          >
            Invia
          </button>
          {isLogged && isAdmin ? (
            ""
          ) : (
            <Link to="/login" className="text-center-white">
              Torna al login
            </Link>
          )}
        </form>
      </div>
    </div>
  );
}
