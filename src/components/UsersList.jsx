import { useContext, useEffect, useState } from "react";
import { UserContext } from "../context/UserContext";
import { Link } from "react-router-dom";
import Cookies from "../../node_modules/js-cookie";

export default function UsersList() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);
  const [utenti, setUtenti] = useState([]); //Per la gestione degli utenti

  useEffect(() => {
    async function fetchUtenti() {
      //Fetch che restituisce la lista utenti
      const response = await fetch(
        "http://localhost:8080/api/utente/getUtenti"
      );
      if (!response.ok) {
        console.log("Errore");
      } else {
        const data = await response.json();
        const utenti = data.map((utente) => ({
          id: utente.id,
          nome: utente.nome,
          cognome: utente.cognome,
          email: utente.email,
          ruolo: utente.ruoli.map((ruolo) => ruolo.tipologia),
        }));
        setUtenti(utenti);
      }
    }
    fetchUtenti();
  }, []);

  async function deleteUser(idUser) {
    //Fetch per cancellare un utente tramite l'id
    const response = await fetch(
      "http://localhost:8080/api/utente/delete/" + idUser, //Endpoint aggiunto
      {
        method: "DELETE",
        headers: {
        Authorization: "Bearer " + Cookies.get("token")
        }
      }
    );

    //console.log("Token delete user: " + Cookies.get("token"));

    if (response.ok) {
      setUtenti(utenti.filter((utente) => utente.id !== idUser)); //Lo cancello anche dalla lista
      alert("Utente cancellato con successo.");
    } else {
      alert("Errore nella cancellazione dell'utente.");
    }
  }

  //Se un utente non è loggato e non è admin la pagina conterrà solamente il messaggio "Non hai le autorizzazioni valide" e un link per tornare alla home
  //Se l'utente è loggato e admin allora può visualizzare tutte gli utenti registrati
  return (
    <div className="container">
      <h1 className="mt-4" style={{ color: "white" }}>
        Utenti registrati
      </h1>
      {isLogged && isAdmin ? (
        <div>
          <div className="row mt-3">
            {utenti.map((user, index) => (
              <div key={index} className="col-md-4 mb-3">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">
                      {user.nome} {user.cognome}
                    </h5>
                    <p className="card-text">Email: {user.email}</p>
                    <span className="badge bg-primary">
                      {user.ruolo.join(", ")}
                    </span>
                  </div>
                  <button
                    className="btn btn-warning"
                    onClick={() => deleteUser(user.id)}
                  >
                    Cancella utente
                  </button>
                </div>
              </div>
            ))}
          </div>
          <div className="mt-3">
            <Link to="/userRegister" className="btn btn-success me-2">
              Aggiungi un utente
            </Link>
          </div>
        </div>
      ) : (
        <div>
          <p>Non hai le autorizzazioni valide</p>
          <Link to="/" className="btn btn-dark">
            Torna alla home
          </Link>
        </div>
      )}
      <br />
    </div>
  );
}
