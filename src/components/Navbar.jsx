import { Link } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "../context/UserContext";
import Cookies from "../../node_modules/js-cookie";

export default function Navbar() {
  const [
    userData,
    setUserData,
    isLogged,
    setIsLogged,
    isAdmin,
    setIsAdmin
  ] = useContext(UserContext);

  //Funzione di logout per il bottone presente nella navbar quando l'utente è loggato
  const logout = () => {
    Cookies.remove("nome");
    Cookies.remove("cognome");
    Cookies.remove("email");
    Cookies.remove("ruoli");
    Cookies.remove("token");

    setUserData({});
    setIsLogged(false);
  };

  return (
    //Se l'utente è loggato mostro il bottone Profilo che porta alla pagina del profilo utente, altrimenti lo nascondo
    //Se l'utente è loggato mostro il bottone di logout altrimenti mostro il bottone di login
    //Se l'utente è loggato ed è Admin mostro i bottoni Corsi e Utenti che portano alla lista dei corsi e alla lista degli utenti
    //Il bottone di logout effettua il logout e porta alla home, il bottone di login porta alla pagina con il form di login
    <nav
      className="navbar navbar-expand-lg navbar-light bg-secondary fixed-top"
      style={{ opacity: 0.7 }}
    >
      <div className="container-fluid">
        <a className="navbar-brand" href="#logo">
          Logo
        </a>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/" className="nav-link text-black">
                Home
              </Link>
            </li>
            {isLogged ? (
              <li className="nav-item">
                <Link to="/userProfile" className="nav-link text-black">
                  Profilo
                </Link>
              </li>
            ) : (
              ""
            )}
            {isLogged && isAdmin ? (
              <li className="nav-item">
                <Link to="/courses" className="nav-link text-black">
                  Corsi
                </Link>
              </li>
            ) : (
              ""
            )}
            {isLogged && isAdmin ? (
              <li className="nav-item">
                <Link to="/users" className="nav-link text-black">
                  Utenti
                </Link>
              </li>
            ) : (
              ""
            )}
            <li className="nav-item">
              {isLogged ? (
                <Link to="/">
                  <button onClick={logout} className="btn btn-dark">
                    Logout
                  </button>
                </Link>
              ) : (
                <Link to="/login" className="nav-link text-black">
                  Login
                </Link>
              )}
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
