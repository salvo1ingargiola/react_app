export default function Footer() {
  //Footer della pagina
  return (
    <footer className="fixed-bottom bg text-dark">
      <div className="container">
        <div className="row">
          <div className="col-md-5">
            <p style={{ color: "white" }}>
              &copy; 2024 AziendaCorsi. Tutti i diritti riservati.
            </p>
          </div>
          <div className="col-md-6">
            <p className="float-md-end" style={{ color: "white" }}>
              Contattaci:{" "}
              <a href="mailto:aziendaCorsi@corsi.com">
                azienda.corsi@corsi.com
              </a>
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}
