export default function NotFound() {
  //Page che stampa al centro della pagina 404 - Pagina non trovata
  return (
    <div style={{ textAlign: "center", padding: "50px" }}>
      <h1 style={{ fontSize: "80px", color: "black" }}>404</h1>
      <p style={{ fontSize: "20px", color: "black" }}>Pagina non trovata</p>
    </div>
  );
}
