import Navbar from "../components/Navbar";
import CourseCard from "../components/CourseCard";

export default function Homepage() {
  return (
    <>
      <Navbar />
      <br />
      <br />
      <CourseCard />
    </>
  );
}
