import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.js";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import { createBrowserRouter } from "react-router-dom";
import { RouterProvider } from "react-router-dom";
import Login from "./pages/Login.jsx";
import Register from "./pages/Register.jsx";
import Homepage from "./pages/Homepage.jsx";
import Layout from "./pages/Layout.jsx";
import UserProfile from "./pages/UserProfile.jsx";
import UserContextProvider from "../src/context/UserContextProvider.jsx";
import Corsi from "./pages/Courses.jsx";
import AddCorso from "./pages/AddCorso.jsx";
import Users from "./pages/Users.jsx";
import NotFound from "./pages/NotFound.jsx";

const router = createBrowserRouter([
  {
    //Wrappo gli elementi nel Context per la gestione degli utenti/admin loggati
    element: (
      <UserContextProvider>
        <Layout />
      </UserContextProvider>
    ),
    children: [
      //Layout sarà presente per tutti i children
      {
        path: "/",
        children: [
          { path: "", element: <Homepage /> },
          { path: "login", element: <Login /> },
          { path: "userRegister", element: <Register /> },
          { path: "userProfile", element: <UserProfile /> },
          { path: "courses", element: <Corsi /> },
          { path: "addCourse", element: <AddCorso /> },
          { path: "users", element: <Users /> },
          { path: "*", element: <NotFound /> },
        ],
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router}></RouterProvider>
  </React.StrictMode>
);
