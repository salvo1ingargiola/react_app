import { useState } from "react";
import { UserContext } from "../context/UserContext";

export default function UserContextProvider({ children }) {
  //Dati standard di un utente
  const [userData, setUserData] = useState({
    nome: "",
    cognome: "",
    email: "",
    ruoli: [""],
  });

  //Tengo traccia dell'utente loggato
  const [isLogged, setIsLogged] = useState(false);

  //Tengo traccia dell'utente admin
  const [isAdmin, setIsAdmin] = useState(false);

  //Tengo traccia del token
  //const [token, setToken] = useState("");

  return (
    <UserContext.Provider
      value={[userData, setUserData, isLogged, setIsLogged, isAdmin, setIsAdmin]} //Rendo globali questi dati
    >
      {children}
    </UserContext.Provider>
  );
}
